﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public interface IXMLDatabase
    {
        void spremi(Blagajna blagajna);
        Blagajna ucitaj();
    }
}
