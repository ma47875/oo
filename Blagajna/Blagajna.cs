﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Blagajna
{
    public class Blagajna : IBlagajna
    {
        public List<KolicinskiArtikl> kolicinskiArtikli { get; set; }
        public List<TezinskiArtikl> tezinskiArtikli { get; set; }
        public List<Korisnik> korisnici { get; set; }
        public List<Racun> racuni { get; set; }
        private Korisnik trenutniKorisnik { get; set; }
        private IXMLDatabase baza;
        private Blagajna blagajna { get; set; }
        private IEkran ekran { get; set; }
        private IIzvjestaj izvjestaj;

        public Blagajna()
        {
            kolicinskiArtikli = new List<KolicinskiArtikl>();
            tezinskiArtikli = new List<TezinskiArtikl>();
            korisnici = new List<Korisnik>();
            racuni = new List<Racun>();
            baza = new XMLDatabase();
            ekran = new Ekran();
            izvjestaj = new Izvjestaj();
        }
        public Blagajna(List<KolicinskiArtikl> kolicinskiArtikli, List<TezinskiArtikl> tezinskiArtikli, List<Korisnik> korisnici)
        {
            baza = new XMLDatabase();
            this.kolicinskiArtikli = kolicinskiArtikli;
            this.tezinskiArtikli = tezinskiArtikli;
            this.korisnici = korisnici;
        }

        public void prijavaKorisnika()
        {
            ekran.ispisiStringURedku("Upišite korisničko ime: ");
            string korisnickoIme = ekran.ucitajRedak();
            ekran.ispisiStringURedku("Upišite šifru: ");
            string sifra = ekran.ucitajRedak();
            trenutniKorisnik = Korisnik.postojiKorisnik(korisnici, korisnickoIme, sifra);
        }

        public void dodajArtikl()
        {
            string redak;
            int broj = -1;
            string naziv;
            float cijena;
            int stopaPDVa;
            while (broj != 0 && broj != 1)
            {
                ekran.ispisiStringURedku("Unesite mjernu jedinicu: 0(kg) ili 1(komad) --");
                redak = Console.ReadLine();
                Int32.TryParse(redak, out broj);
            }
            ekran.ispisiStringURedku("Unesite naziv artikla : ");
            naziv = ekran.ucitajRedak();
            do
            {
                cijena = -1;
                ekran.ispisiStringURedku("Unesite cijenu artikla : ");
                redak = ekran.ucitajRedak();
                try
                {
                    cijena = float.Parse(redak);
                }
                catch
                {
                    ekran.ispisiString("Molim unesite ipravanu cijenu");
                    continue;
                }
            } while (cijena<0);

            do
            {
                stopaPDVa = -1;
                ekran.ispisiStringURedku("Unesite stopu PDV-a : ");
                redak = ekran.ucitajRedak();
                try
                {
                    stopaPDVa = Int32.Parse(redak);
                }
                catch
                {
                    ekran.ispisiString("Molim unesite ipravanu stopu PDVa!");
                    continue;
                }
            } while (stopaPDVa < 0 && stopaPDVa>100);
            
            
            if (broj == 0)
            {
                tezinskiArtikli.Add(new TezinskiArtikl(naziv, cijena, stopaPDVa));
            }
            else
            {
                kolicinskiArtikli.Add(new KolicinskiArtikl(naziv, cijena, stopaPDVa));
            }
            baza.spremi(this);
        }

        public void izradiRacun()
        {
            Racun r = new Racun();
            string redak;
            int broj;
            float kolicina;
            int odluka = -1;
            int ukupnoArtikala=Artikl.ukupnoArtikala(tezinskiArtikli, kolicinskiArtikli);
            do
            {
                Artikl.ispisiSveArtikle(tezinskiArtikli, kolicinskiArtikli);
                do
                {
                    odluka = -1;
                    broj = -1;
                    ekran.ispisiStringURedku("Redni broj artikla:");
                    redak = ekran.ucitajRedak();
                    try
                    {
                        broj = Int32.Parse(redak);
                    }
                    catch
                    {
                        ekran.ispisiString("Molim unesite ipravanu broj artikla!");
                        continue;
                    }
                } while (broj < 0 || broj > ukupnoArtikala);
                
                if (broj > tezinskiArtikli.Count() - 1)
                {
                    Console.Write("Unesite količinu:");
                    redak = Console.ReadLine().Trim();
                    float.TryParse(redak, out kolicina);
                    r.kolicinskeStavke.Add(new KolicinskaStavka(kolicinskiArtikli.ElementAt(broj - tezinskiArtikli.Count()), (int)kolicina));
                }
                else
                {
                    Console.Write("Unesite težinu:");
                    redak = Console.ReadLine().Trim();
                    float.TryParse(redak, out kolicina);
                    r.tezinskeStavke.Add(new TezinskaStavka(tezinskiArtikli.ElementAt(broj), kolicina));
                }
                while (odluka != 0 && odluka != 1)
                {
                    ekran.ispisiStringURedku("0-Dodaj sljedeći artikl\n1-Kreiraj račun\nRedni broj željene aktivnosti:");
                    redak = Console.ReadLine();
                    try
                    {
                        odluka = Int32.Parse(redak);
                    }
                    catch
                    {
                        ekran.ispisiString("Molim unesite ipravan broj aktivnosti!");
                        continue;
                    }
                }
            } while (odluka == 0);
            r.zavrsiRacun();
            racuni.Add(r);
            r.ispisiRacun();
            baza.spremi(this);
        }

        public void izvjestajDana()
        {
            List<Racun> racuniDana = Racun.vratiRacuneDanasnjegDana(racuni);
            izvjestaj.izvjestajDana(racuniDana);

        }

        public void izvjestajArtikli()
        {
            izvjestaj.izvjestajArtikli(racuni);

        }

        public void izbrisiRacun()
        {
            int broj = - 1;
            string redak;
            int ukupno = Racun.ukupnoRacuna(racuni);
            Racun.ispisiRacune(racuni);
            do
            {
                broj = -1;
                ekran.ispisiStringURedku("Redni broj racuna:");
                redak = ekran.ucitajRedak();
                try
                {
                    broj = Int32.Parse(redak);
                }
                catch
                {
                    ekran.ispisiString("Molim unesite ipravanu broj racuna!");
                    continue;
                }
            } while (broj < 0 || broj > ukupno);
            Racun.izbrisiRacun(racuni, broj);
            baza.spremi(this);
        }

        public void pokreniBlagajnu()
        {
            int aktivnost = -1;
            string redak;
            Blagajna blagajna = baza.ucitaj();
            while (blagajna.trenutniKorisnik == null)
            {
                blagajna.prijavaKorisnika();
            }
            while (aktivnost != 3)
            {
                Console.Write(blagajna.trenutniKorisnik.vratiMogucnosti());
                Console.Write("\nRedni broj željene aktivnosti:");
                redak = Console.ReadLine().Trim();
                try
                {
                    aktivnost = Int32.Parse(redak);
                }
                catch
                {
                    Console.WriteLine("Molim unesite ipravan broj akivnosti!");
                    continue;
                }
                if (!blagajna.trenutniKorisnik.imaOvlasti(aktivnost))
                {

                    Console.WriteLine("Nemate ovlasti");
                    continue;
                }
                switch (aktivnost)
                {
                    case 0:
                        blagajna.izradiRacun();
                        break;
                    case 1:
                        blagajna.izvjestajDana();
                        break;
                    case 2:
                        blagajna.izvjestajArtikli();
                        break;
                    case 3:
                        break;
                    case 4:
                        blagajna.dodajArtikl();
                        break;
                    case 5:
                        blagajna.izbrisiRacun();
                        break;
                    default:
                        break;

                }
            }
        }
    }
}
