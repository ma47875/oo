﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Blagajna
{
    public class XMLDatabase : IXMLDatabase
    {
        public readonly string datoteka = "baza.xml";
        XmlSerializer ser = new XmlSerializer(typeof(Blagajna));

        public void spremi(Blagajna blagajna)
        {
            TextWriter writer = new StreamWriter(datoteka);
            ser.Serialize(writer, blagajna);
            writer.Close();
        }

        public Blagajna ucitaj()
        {
            FileStream fs = new FileStream(datoteka, FileMode.Open);
            Blagajna blagajna;
            blagajna = (Blagajna)ser.Deserialize(fs);
            fs.Close();
            return blagajna;
        }
        public void spremiArtikl(Blagajna blagajan, Artikl artikl)
        {

        }
    }
}
