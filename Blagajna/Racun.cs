﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class Racun
    {
        public List<KolicinskaStavka> kolicinskeStavke { get; set; }
        public List<TezinskaStavka> tezinskeStavke { get; set; }
        public DateTime vrijeme { get; set; }
        public double ukupanIznos { get; set; }
        public double iznosPDV { get; set; }
        private IEkran ekran;

        public Racun()
        {
            kolicinskeStavke = new List<KolicinskaStavka>();
            tezinskeStavke = new List<TezinskaStavka>();
            ekran = new Ekran();
        }

        public void zavrsiRacun()
        {
            izracunajIznosPDVa();
            izracunajUkupanIznos();
            vrijeme = DateTime.Now;
        }

        private void izracunajUkupanIznos()
        {
            double suma = 0f;
            foreach (KolicinskaStavka stavka in kolicinskeStavke)
            {
                suma += stavka.cijena;
            }
            foreach (TezinskaStavka stavka in tezinskeStavke)
            {
                suma += stavka.cijena;
            }
            ukupanIznos = Math.Round(suma, 2);
        }

        private void izracunajIznosPDVa()
        {
            double suma = 0f;
            foreach (KolicinskaStavka stavka in kolicinskeStavke)
            {
                suma += stavka.cijena*stavka.artikl.stopaPDVa / 100;
            }
            foreach (TezinskaStavka stavka in tezinskeStavke)
            {
                suma += stavka.cijena * stavka.artikl.stopaPDVa / 100;
            }
            iznosPDV = Math.Round(suma, 2);
        }

        public void ispisiRacun()
        {
            int redniBrojArtikla = 0;
            string racun = "\n";
            foreach (TezinskaStavka ts in tezinskeStavke)
            {
                racun += redniBrojArtikla + "." + ts.artikl.naziv + " - Težina:" + ts.tezina + " - Cijena:" + ts.cijena + " kn - PDV:" + ts.artikl.stopaPDVa + "%\n";
                redniBrojArtikla++;
            }
            foreach (KolicinskaStavka ts in kolicinskeStavke)
            {
                racun += redniBrojArtikla + "." + ts.artikl.naziv + " - Količina:" + ts.kolicina + " - Cijena:" + ts.cijena + " kn - PDV:" + ts.artikl.stopaPDVa + "%\n";
                redniBrojArtikla++;
            }
            racun += "Ukupna cijena:" + ukupanIznos + " kn\n";
            racun += "Iznos PDVa:" + iznosPDV + " kn\n";
            racun += "Vrijeme izdavanja:" + vrijeme.ToLocalTime() + "\n";
            ekran.ispisiString(racun);
        }

        public static List<Racun> vratiRacuneDanasnjegDana(List<Racun> racuni)
        {
            List<Racun> racuniDana = new List<Racun>();
            foreach (Racun r in racuni)
            {
                if (r.vrijeme.Date.Equals(DateTime.Now.Date))
                {
                    racuniDana.Add(r);
                }
            }
            return racuniDana;
        }

        public int brojArtikalaNaRacunu()
        {
            return kolicinskeStavke.Count() + tezinskeStavke.Count();
            
        }

        public static double zbrojSvihRacuna(List<Racun> racuni)
        {
            double iznos = 0f;
            foreach(Racun racun in racuni)
            {
                iznos += racun.ukupanIznos;
            }
            return iznos;
        }

        public static void ispisiRacune(List<Racun> racuni)
        {
            IEkran ekran = new Ekran();
            int i = 0;
            foreach(Racun r in racuni)
            {
                ekran.ispisiStringURedku(i + ".");
                r.ispisiRacun();
                i++;
            }
        }
        public static void izbrisiRacun(List<Racun> racuni, int i)
        {
            racuni.RemoveAt(i);
        }

        public static int ukupnoRacuna(List<Racun> racuni)
        {
            return racuni.Count()-1;
        }
    }
}
