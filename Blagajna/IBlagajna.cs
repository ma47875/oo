﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    interface IBlagajna
    {
        void prijavaKorisnika();

        void dodajArtikl();

        void pokreniBlagajnu();
        void izradiRacun();

        void izbrisiRacun();
    }
}
