﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class Izvjestaj: IIzvjestaj
    {
        private IEkran ekran;

        public Izvjestaj()
        {
            ekran = new Ekran();
        }
        public void izvjestajDana(List<Racun> racuni)
        {
            int i = 0;
            string ispis = "";
            ispis += "Danas je izdano " + racuni.Count() + " račun/a.\n";
            ispis += "Ukupan iznos svih računa:" + Racun.zbrojSvihRacuna(racuni) + " kn\n";
            foreach (Racun r in racuni)
            {
                ispis += "Racun br." + (i + 1) + " Vrijeme: " + r.vrijeme.ToLocalTime() + "  Artikala broj:" + (r.brojArtikalaNaRacunu()) + "  Ukupan iznos:" + r.ukupanIznos + " kn\n";
                i++;
            }
            ekran.ispisiString(ispis);
        }
        public void izvjestajArtikli(List<Racun> racuni)
        {
            Dictionary<string, int> rijecnik = new Dictionary<string, int>();
            foreach (Racun r in racuni)
            {
                foreach (TezinskaStavka ts in r.tezinskeStavke)
                {
                    if (!rijecnik.ContainsKey(ts.artikl.naziv))
                    {
                        rijecnik[ts.artikl.naziv] = 1;
                    }
                    else
                    {
                        rijecnik[ts.artikl.naziv]++;
                    }
                }
                foreach (KolicinskaStavka ts in r.kolicinskeStavke)
                {
                    if (!rijecnik.ContainsKey(ts.artikl.naziv))
                    {
                        rijecnik[ts.artikl.naziv] = 1;
                    }
                    else
                    {
                        rijecnik[ts.artikl.naziv]++;
                    }
                }
            }
            List<KeyValuePair<string, int>> lista = rijecnik.ToList();
            lista.Sort(Compare);
            string ispis = "\n";
            foreach(KeyValuePair<string, int> kp in lista)
            {
                ispis += kp.Key + " - " + kp.Value + " komad/a\n";
            }
            ekran.ispisiString(ispis);
        }

        static int Compare(KeyValuePair<string, int> a, KeyValuePair<string, int> b)
        {
            return b.Value.CompareTo(a.Value);
        }
    }
}
