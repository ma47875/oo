﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public abstract class Artikl
    {
        public string naziv { get; set; }
        public float cijena { get; set; }
        public string mjernaJedinica { get; set; }
        public int stopaPDVa { get; set; }

        public Artikl(string naziv, float cijena,string mjernaJedinica, int stopaPDVa)
        {
            this.naziv = naziv;
            this.cijena = cijena;
            this.mjernaJedinica = mjernaJedinica;
            this.stopaPDVa = stopaPDVa;
        }
        public Artikl(){
        }

        public abstract double izracunajCijenu(float kol);

        public static void ispisiSveArtikle(List<TezinskiArtikl> tezinskiArtikli, List<KolicinskiArtikl> kolicinskiArtikli)
        {
            IEkran ekran = new Ekran();
            int i = 0;
            foreach (Artikl a in tezinskiArtikli)
            {
                ekran.ispisiString(i + "." + a.naziv);
                i++;
            }
            foreach (Artikl a in kolicinskiArtikli)
            {
                ekran.ispisiString(i + "." + a.naziv);
                i++;
            }
        }

        public static int ukupnoArtikala(List<TezinskiArtikl> tezinskiArtikli, List<KolicinskiArtikl> kolicinskiArtikli)
        {
            return tezinskiArtikli.Count() + kolicinskiArtikli.Count() - 1;
        }
    }
}
