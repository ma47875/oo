﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    interface IEkran
    {
        void ispisiString(string s);
        void ispisiStringURedku(string s);
        string ucitajRedak();
    }
}
