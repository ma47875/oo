﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class TezinskiArtikl:Artikl
    {

        public TezinskiArtikl() : base() { }
        public TezinskiArtikl(string naziv, float cijena, int stopaPDVa) : base(naziv, cijena, "kg", stopaPDVa) { }

        public override double izracunajCijenu(float kolicina)
        {
            return Math.Round(kolicina * cijena ,2);
        }
    }
}
