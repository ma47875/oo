﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class Korisnik
    {
        public string korisnickoIme{get;set;}
        public string sifra { get; set; }
        public bool jeAdmin { get; set; }
        private static List<string> mogucnosti { get; set; }

        public Korisnik(string korisnickoIme, string sifra, bool jeAdmin)
        {
            this.korisnickoIme = korisnickoIme;
            this.sifra = sifra;
            this.jeAdmin = jeAdmin;
            mogucnosti = new List<string>();
            mogucnosti.Add("Izradi račun");
            mogucnosti.Add("Ispis izvještaja za dan");
            mogucnosti.Add("Statistika artikala");
            mogucnosti.Add("Kraj rada");
            mogucnosti.Add("Dodaj artikl");
            mogucnosti.Add("Brisanje računa");
        }
        public Korisnik() {
            mogucnosti = new List<string>();
            mogucnosti.Add("Izradi račun");
            mogucnosti.Add("Ispis izvještaja za dan");
            mogucnosti.Add("Statistika artikala");
            mogucnosti.Add("Kraj rada");
            mogucnosti.Add("Dodaj artikl");
            mogucnosti.Add("Brisanje računa");
        }

        public bool imaOvlasti(int mogucnost)
        {
            if(mogucnost >2 && mogucnost < 6)
            {
                if (this.jeAdmin==true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public string vratiMogucnosti()
        {
            string ispis = "";
            int inkrement = 0;
            int granica = 4;
            if (jeAdmin)
            {
                granica += 2 ;
            }
            while (inkrement < granica)
            {
                ispis+= inkrement+" : "+ mogucnosti.ElementAt(inkrement)+"\n";
                inkrement++;
            }
            return ispis;
        }

        public static Korisnik postojiKorisnik(List<Korisnik> korisnici, string korisnickoIme, string sifra)
        {
            foreach (Korisnik korisnik in korisnici)
            {
                if (korisnik.korisnickoIme.Equals(korisnickoIme))
                {
                    if (korisnik.sifra.Equals(sifra))
                    {
                        return korisnik;
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            return null;
        }
    }
}
