﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Blagajna
{
    class Program
    {
        static void Main(string[] args)
        {
            IBlagajna bb = new Blagajna();
            bb.pokreniBlagajnu();
        }
    }
}
