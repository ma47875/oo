﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class KolicinskiArtikl:Artikl
    {

        public KolicinskiArtikl() : base() { }
        public KolicinskiArtikl(string naziv, float cijena, int stopaPDVa) : base(naziv, cijena, "komad", stopaPDVa) { }

        public override double izracunajCijenu(float kol)
        {
            return Math.Round((int)kol * cijena, 2);
        }
    }
}
