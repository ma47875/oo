﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class KolicinskaStavka
    {
        public KolicinskiArtikl artikl { get; set; }
        public int kolicina { get; set; }
        public double cijena { get; set; }

        public KolicinskaStavka(KolicinskiArtikl artikl, int kolicina)
        {
            this.artikl = artikl;
            this.kolicina = kolicina;
            this.cijena = artikl.izracunajCijenu(kolicina);
        }
        public KolicinskaStavka() { }
    }
}
