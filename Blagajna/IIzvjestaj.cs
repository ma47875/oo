﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    interface IIzvjestaj
    {
        void izvjestajDana(List<Racun> racuni);
        void izvjestajArtikli(List<Racun> racuni);
    }
}
