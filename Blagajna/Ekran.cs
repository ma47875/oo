﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class Ekran:IEkran
    {
        public void ispisiString(string s)
        {
            Console.WriteLine(s);
        }

        public void ispisiStringURedku(string s)
        {
            Console.Write(s);
        }
        public string ucitajRedak()
        {
            return Console.ReadLine().Trim();
        }
    }
}
