﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blagajna
{
    public class TezinskaStavka
    {
        public TezinskiArtikl artikl { get; set; }
        public float tezina { get; set; }
        public double cijena { get; set; }

        public TezinskaStavka(TezinskiArtikl artikl, float tezina)
        {
            this.artikl = artikl;
            this.tezina = tezina;
            this.cijena = artikl.izracunajCijenu(tezina);
        }
        public TezinskaStavka() { }

    }
}

